package io.git.kadir.jersey.example.util;

/**
 * @author akarakoc
 * Date :   10.05.2019
 * Time :   09:39
 */
public class Constants {
    public static final String ENTER_REST_SERVICE_KEY = "client-enter-time-in-ms";
    public static final String REQUEST_ID_KEY = "request-id";
    public static final String REMOTE_ADDRESS_KEY = "remote-address";
    public static final String REMOTE_PORT_KEY = "remote-port";
    public static final String REMOTE_HOST_KEY = "remote-host";

    private Constants() {
    }
}
